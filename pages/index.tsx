import React, { Suspense, useEffect } from "react";
import Footer from "@components/Footer";
import Image from "next/image";
import { faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Lottie from "lottie-react";
import animationCode from "@animations/code-animation.json";
import { ToastContainer } from "react-toastify";
import { myLoader } from "@utils/loader";
import dynamic from "next/dynamic";
import Header from "@components/Header";
const Contact = dynamic(() => import("../components/Contact"), { ssr: false });

const Home = () => {
  useEffect(() => {
    const path = document.getElementsByTagName("path");
    for (let i = 0; i < path.length; i++) {
      path[i].setAttribute("alt", "icon");
    }
  }, []);

  return (
    <>
      <Header />
      <ToastContainer />
      <section>
        <div className="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
          <div className="container">
            <div className="row">
              <div className="col-lg-12">
                <div className="row">
                  <div className="col-lg-12 align-self-center">
                    <div className="left-content header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
                      <h1 className="display-1">
                        Création de <span className="color-blue">sites web</span> et <br />
                        <span className="color-blue">d'applications mobile.</span>
                        <br />
                        <span>
                          {" "}
                          Agence digitale sur Paris, <span className="color-blue">Petites ou Moyennes entreprises, Associations</span> et{" "}
                          <span className="color-blue">Professions libérales.</span>
                        </span>
                      </h1>
                      <br />
                    </div>
                    <hr />
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <section>
        <div className="container">
          <div className="col-xl-12">
            <h2 className="display-4">
              Besoin de <span className="color-bleu">créer</span> ou <span className="color-bleu"> de moderniser</span> votre identitée
              <span className="color-bleu"> digitale</span> ?<br />
            </h2>
          </div>
        </div>
      </section>

      {/* ####################################################################
                                   NOS SERVICES 
     #################################################################### */}
      <div id="services" className="section our-services">
        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <h3>Nos services</h3>
              <br />
              <br />
              <p>
                Nous travaillons avec nos clients comme une équipe. Notre méthode est définie en 4 grandes étapes : définir le besoin, conceptualiser
                votre demande, développer la solution avec les technologies modernes les plus adaptées afin de pouvoir réaliser un site durable et
                optimisé ainsi qu'une phase de tests pour corriger les éventuels problèmes avant la mise en production.
              </p>
            </div>
            <div className="col-md-6">
              <Lottie
                alt="Animation indiquant la bonne récéption du courriel"
                animationData={animationCode}
                autoPlay
                loop
                style={{ width: 400, margin: "0 auto" }}
              />
            </div>
          </div>
          <br />
          <br />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-md-6">
              <h4 className="text-center">
                UX/UI Design <FontAwesomeIcon alt="icon check" icon={faCheck} style={{ width: 25, color: "green", marginLeft: 5 }} />
              </h4>
              <br />
              <p className="text-center medium">
                Confiez-nous votre identité de marque ou votre logo. Notre agence web est spécialisée en Web Design et création graphique.
              </p>
              <br />
            </div>
            <div className="col-md-6">
              <h4 className="text-center">
                Développement <FontAwesomeIcon alt="icon check" icon={faCheck} style={{ width: 25, color: "green", marginLeft: 5 }} />
              </h4>
              <br />
              <p className="text-center">Nous répondons à tous les projets web (sites vitrines, e-commerces, applications mobiles).</p>
              <br />
            </div>
            <div className="col-md-6">
              <h4 className="text-center">
                E-commerce <FontAwesomeIcon alt="icon check" icon={faCheck} style={{ width: 25, color: "green", marginLeft: 5 }} />
              </h4>
              <br />
              <p className="text-center">
                Emailing, marketing automation, parcours digitaux innovants, nous mettons notre savoir faire pour augmenter vos ventes et accroître
                votre retour sur investissement.
              </p>
              <br />
            </div>
            <div className="col-md-6">
              <h4 className="text-center">
                Référencement <FontAwesomeIcon alt="icon check" icon={faCheck} style={{ width: 25, color: "green", marginLeft: 5 }} />
              </h4>
              <br />
              <p className="text-center">
                Que ce soit par le référencement naturel ou par Google Adwords afin d’être en première position, nous générons du trafic pour
                augmenter vos ventes.
              </p>
              <br />
            </div>
          </div>
        </div>
      </div>

      {/* ####################################################################
                                   NOS COMPETENCES 
     #################################################################### */}
      <div id="competences" className="about-us section">
        <div className="container">
          <h3>Nos compétences</h3> <br />
          <br />
          <div className="row">
            <div className="col-lg-4">
              <div className="d-none d-sm-block left-image wow fadeIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <Suspense fallback={<div />}>
                  <Image loader={myLoader} src="/assets/img/img-a.png" alt="person graphic" width={375} height={375} quality={25} />
                </Suspense>
              </div>
            </div>
            <div className="col-lg-8 col-xs-12 align-self-center">
              <div className="services">
                <div className="row">
                  <div className="gestion-service col-lg-6">
                    <div className="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.5s">
                      <div className="d-none d-sm-block icon">
                        <Suspense fallback={<div />}>
                          <Image loader={myLoader} src="/assets/images/service-icon-01.png" alt="reporting" width={70} height={70} quality={100} />
                        </Suspense>
                      </div>
                      <div className="right-text">
                        <h4>Base de données</h4>
                        <p className="d-block d-sm-none">
                          Le système de gestion de base de données est utilisé afin de stocker les données générales du site web. <br />
                          La couche logicielle de stockage est divisée en 2 grandes familles, les bases de données relationnelles et non
                          relationnelles. Cette difference est dûe à l'arrivée de MongoDB qui est aujourd'hui un acteur important. <br />
                          Nous avons ensuite les bases de données relationnelles avec MySQL et MariaDB qui sont des acteurs principaux notamment pour
                          les sites WordPress.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="gestion-service col-lg-6">
                    <div className="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
                      <div className="d-none d-sm-block icon">
                        <Suspense fallback={<div />}>
                          <Image
                            loader={myLoader}
                            src="/assets/images/service-icon-02.png"
                            alt="service site web/application mobile"
                            width={70}
                            height={70}
                            quality={100}
                          />
                        </Suspense>
                      </div>
                      <div className="right-text">
                        <h4>Création Site web / Application mobile</h4>
                        <p className="d-block d-sm-none">
                          La création de sites web est totalement personnalisée. <br />
                          Selon votre objectif, un expert en markerting vous conseillera et notre expert en e-business vous accompagnera pour les
                          atteindre.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="gestion-service col-lg-6">
                    <div className="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.1s">
                      <div className="d-none d-sm-block icon">
                        <Suspense fallback={<div />}>
                          <Image loader={myLoader} src="/assets/images/service-icon-04.png" alt="service seo" width={70} height={70} quality={100} />
                        </Suspense>
                      </div>
                      <div className="right-text">
                        <h4>SEO</h4>
                        <p className="d-block d-sm-none">
                          On l’appelle aussi référencement naturel.
                          <br />
                          Notre objectif est d’améliorer la visibilité de votre site sur les moteurs de recherche. Le but est de faire se rencontrer
                          les internautes intéressés par vos produits / services ou du contenu informatif.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="gestion-service col-lg-6">
                    <div className="item wow fadeIn" data-wow-duration="1s" data-wow-delay="0.9s">
                      <div className="d-none d-sm-block icon">
                        <Suspense fallback={<div />}>
                          <Image
                            loader={myLoader}
                            src="/assets/images/service-icon-03.png"
                            alt="service hébergement"
                            width={70}
                            height={70}
                            quality={100}
                          />
                        </Suspense>
                      </div>
                      <div className="right-text">
                        <h4>Hébergement</h4>
                        <p className="d-block d-sm-none">
                          L'hébergement vous permet de visualiser et de stocker les informations de votre site. Des solutions existes : Firebase,
                          Amazon Web Service, IONIO et 1&1 qui vous permets à moindre coup d'héberger votre site.
                          <br />
                          Le site web peut être hébergé sur 2 types d'hébergements, le serveur mutualisé et individuel.
                          <br />
                          L'avantage du serveur mutualisé vous permets d'avoir un coup réduit sur le prix mais la puissance est relativement réduite.
                          Une solution idéale pour les sites vitrines.
                          <br />
                          Pour les serveurs induividuels, vous devez tout personnaliser. C'est une solution plus couteuse car elle doit être
                          configurée par un professionel.
                          <br />
                          Le choix du serveur est très important et mérite réflexion en fonction de vos besoins et du traffic internet qu'il va
                          engendrer.
                          <br />
                          Notre objectif est de déterminer la solution la plus optimale entre le prix et la puissance.
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="gestion-service offset-lg-6">
                    <div className="item wow fadeIn" data-wow-duration="1s" data-wow-delay="1.3s">
                      <div className="d-none d-sm-block icon">
                        <Suspense fallback={<div />}>
                          <Image
                            loader={myLoader}
                            src="/assets/images/service-icon-03.png"
                            alt="service gestion de vos réseaux"
                            width={70}
                            height={70}
                            quality={100}
                          />
                        </Suspense>
                      </div>
                      <div className="right-text">
                        <h4>Gestion de vos réseaux sociaux</h4>
                        <p className="d-block d-sm-none">
                          Avant de communiquer sur vos réseaux, notre Community Manager analysera votre marché et vos concurrents.
                          <br />
                          Ce benchmark nous permet de mieux connaitre votre environnement et d’en déduire les « bonnes pratiques » de votre secteur.
                          <br />
                          Nous identifions vos cibles tout en plaçant des objectifs de fréquentation et d’engagement, puis nous déterminons ensemble,
                          les moyens les plus efficaces de les atteindre. Cette étape est indispensable pour vous garantir des résultats positifs.
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* ####################################################################
                                   NOS GARANTIES 
     #################################################################### */}
      <div id="about" className="our-services section">
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <h3>Nos garanties</h3> <br />
              <br />
              <div className="row nos-garanties">
                <div className="col-xl-12 row">
                  <h4>Une équipe de développeurs et d'experts en gestion de projet</h4>
                  <p className="col-xl-12">
                    Nous avons décidé de lancer notre agence pour fournir une solution de qualité en fonction de votre besoin. Nos compétences en
                    développement informatique nous permettent d'utiliser des technologies modernes afin de vous garantir les meilleurs scores de
                    performances (référencement, rapidité de chargement).
                  </p>
                </div>

                <div className="col-xl-12 row">
                  <h4>Mise en place d'une procédure de reprise d'activité d'urgence</h4>
                  <p className="col-xl-12">
                    Tout professionnel doit pouvoir avoir un système d'information irréprochable. Nous accompagnons nos clients peu importe la
                    situation. <br />
                    Nos infrastructures sont basées en France, avec un système de sauvegarde régulier afin de diminuer le risque de perte de données.
                  </p>
                </div>

                <div className="col-xl-12 row">
                  <h4>Confidentialité pour les projets les plus pointus</h4>
                  <p className="col-xl-12">
                    Formés en tant que chef de projet, nous avons appris à mettre en place pour les clients le souhaitant, un contrat de
                    confidentialité. Nous travaillons avec un avocat pour la rédaction et le conseil.
                  </p>
                </div>

                <div className="col-xl-12 row">
                  <h4>Un design unique pour chaque projet</h4>
                  <p className="col-xl-12">
                    Nous avons appris par expérience que UI/UX est le coeur de notre travail Etudier l'utilisateur type ainsi que la création de
                    d'interface avec un design unique, simple et ergonomique.
                  </p>
                </div>
              </div>
              <br />
            </div>
          </div>
        </div>
      </div>

      {/* ####################################################################
                                   QUI SOMMES-NOUS ?
     #################################################################### */}
      <div id="portfolio" className="our-portfolio section">
        <div className="container">
          <div className="row">
            <div className="col-lg-6 offset-lg-3">
              <div className="section-heading wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
                <h3>Qui sommes-nous ?</h3> <br />
                <br />
              </div>
            </div>
          </div>
          <hr />
          <div className="row justify-content-lg-center">
            <div className="col-lg-4 col-sm-6 col-xs-12">
              <a>
                <div className="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                  <div className="hidden-content personnas">
                    <h6>Allan BUSI</h6>
                    <p>Expert en développement web et chef de projet</p>
                  </div>
                  <div className="showed-content">
                    <Suspense fallback={<div />}>
                      <Image loader={myLoader} src="/assets/img/allan.png" alt="allan busi" width={100} height={100} quality={70} />
                    </Suspense>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-lg-4 col-sm-6 col-xs-12">
              <a>
                <div className="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.5s">
                  <div className="hidden-content personnas">
                    <h6>Adrien MAILLARD</h6>
                    <p>Expert en développement web &amp; mobile</p>
                  </div>
                  <div className="showed-content">
                    <Suspense fallback={<div />}>
                      <Image loader={myLoader} src="/assets/img/adrien.png" alt="adrien maillard" width={100} height={100} quality={70} />
                    </Suspense>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-lg-4 col-sm-6 col-xs-12">
              <a>
                <div className="item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                  <div className="hidden-content personnas">
                    <h6>Sarah OURIACHI</h6>
                    <p>ExpertE en SEO</p>
                  </div>
                  <div className="showed-content">
                    <Suspense fallback={<div />}>
                      <Image loader={myLoader} src="/assets/img/sarah.png" alt="sarah ouriachia" width={100} height={100} quality={70} />
                    </Suspense>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </div>

      {/* ####################################################################
                                   CONTACT 
     #################################################################### */}
      <Suspense fallback={<div />}>
        <Contact />
      </Suspense>
      {/* FOOTER */}
      <Footer />
    </>
  );
};

export default Home;
