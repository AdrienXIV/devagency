const Meta = () => {
  return (
    <>
      <meta charSet="utf-8" />
      <meta name="Content-Type" content="UTF-8" />
      <meta name="Content-Language" content="fr" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta
        name="description"
        content="Votre Agence Web basée sur Coulommiers - Rouen - Paris. Créations ou modernisations des sites web pour les professionnels, d'application mobile et de logiciel."
      />
      <meta name="Subject" content="Agence web" />
      <meta name="Copyright" content="Dev Agency" />
      <meta name="Publisher" content="Allan BUSI" />
      <meta name="Identifier-Url" content="https://devagency.vercel.app" />
      <meta name="Revisit-After" content="31 days" />
      <meta name="Robots" content="index" />
      <meta name="Rating" content="general" />
      <meta name="Distribution" content="global" />
      <meta name="Expires" content="12/12/2025" />
      <meta name="Geography" content="Paris" />
      <meta name="Category" content="economics" />
      <meta name="DC.Title" content="DevAgency" />
      <meta name="DC.Content-Type" content="UTF-8" />
      <meta name="DC.Content-Language" content="fr" />
      <meta
        name="DC.Description"
        content="Votre Agence Web basée sur Coulommiers - Rouen - Paris. Créations ou modernisations des sites web pour les professionnels, d'application mobile et de logiciel."
      />
      <meta
        name="DC.Keywords"
        content="Agence, Web, Developpement, Création, Modernisation,Votre, Personnalisation, Rouen, Paris, Coulommiers, HTML, CSS, JavaScript, NextJS, TypeScript, SEO, MongoDB, Base de données, Développeurs, Business, Qualité, Optimisation, ReactJS, NodeJS, SSR"
      />
      <meta name="DC.Subject" content="Agence web" />
      <meta name="DC.Copyright" content="Dev Agency" />
      <meta name="DC.Author" content="Allan BUSI, Adrien Malliard, Sarah Ouriachi" />
      <meta name="DC.Publisher" content="Allan BUSI" />
      <meta name="DC.Identifier-Url" content="https://devagency.vercel.app" />

      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" />
      <meta property="og:url" content="https://devagency.vercel.app" />
      <meta property="og:title" content="DevAgency | Création site web - Application mobile" />
      <meta
        property="og:description"
        content="Votre Agence Web basée sur Coulommiers - Rouen - Paris. Création ou modernisation de sites web pour les professionnels, d'applications mobiles et de logiciels."
      />
      <meta property="og:image" content="https://devagency.vercel.app/assets/images/devagency.png" />

      {/* <!-- Twitter --> */}
      <meta property="twitter:card" content="summary_large_image" />
      <meta property="twitter:url" content="https://devagency.vercel.app" />
      <meta property="twitter:title" content="DevAgency | Création site web - Application mobile" />
      <meta
        property="twitter:description"
        content="Votre Agence Web basée sur Coulommiers - Rouen - Paris. Création ou modernisation de sites web pour les professionnels, d'applications mobiles et de logiciels."
      />
      <meta property="twitter:image" content="https://devagency.vercel.app/assets/images/devagency.png" />

      <meta
        name="keywords"
        content="Agence, Web, Developpement, Création, Modernisation,Votre, Personnalisation, Rouen, Paris, Coulommiers, HTML, CSS, JavaScript, NextJS, TypeScript, SEO, MongoDB, Base de données, Développeurs, Business, Qualité, Optimisation, ReactJS, NodeJS, SSR"
      />
      <meta name="author" content="Adrien MAILLARD, Allan BUSI, Sarah Ouriachi" />
    </>
  );
};

export default Meta;
