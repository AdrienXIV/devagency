import React, { Suspense, useRef, useState } from "react";
import Loader from "react-loader-spinner";
import { faPhone } from "@fortawesome/free-solid-svg-icons";
import axios from "axios";
import Lottie from "lottie-react";
import animationEmail from "@animations/send-email.json";
import dynamic from "next/dynamic";
import { toast } from "react-toastify";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

const ReCAPTCHA: any = dynamic(() => import("react-google-recaptcha"), { ssr: false });

const Contact = () => {
  const recaptchaRef = useRef(null);
  const [state, setState] = useState({
    email: "",
    nom: "",
    prenom: "",
    message: "",
    tel: "",
    captcha: false,
  });
  const [contactSuccess, setContactSuccess] = useState(false);
  const [contactLoad, setContactLoad] = useState(false);

  const onSubmit = async (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    toast.clearWaitingQueue();

    // si le Captcha n'est pas coché
    if (!state.captcha) {
      return toast("Veuillez valider le Captcha", { type: "error", position: "top-center", delay: 1000 });
    }

    setContactLoad(true);
    try {
      const { data } = await axios.post("/api/contact", state);
      setContactSuccess(true);
      setState({
        email: "",
        nom: "",
        prenom: "",
        message: "",
        tel: "",
        captcha: false,
      });
      // on remet à 0 le Captcha
      recaptchaRef.current && recaptchaRef.current.props.grecaptcha.reset();
    } catch (error) {
      Array.isArray(error.response.data)
        ? error.response.data.map(err => {
            toast(err, { type: "warning", position: "top-center", delay: 1000 });
          })
        : toast(error.response.data?.error || "Erreur serveur", { type: "error", position: "top-center", delay: 1000 });
    }
    setContactLoad(false);
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
    const name = e.target.name;
    const value = e.target.value;

    setState(prev => ({ ...prev, [name]: value }));
  };

  return (
    <div id="contact" className="contact-us section">
      <div className="container">
        <div className="row">
          <div className="col-lg-6 align-self-center wow fadeInLeft" data-wow-duration="0.5s" data-wow-delay="0.25s">
            <div className="section-heading nous-sommes-a-lecoute">
              <h3>Nous sommes à l'écoute de vos besoins</h3>
              <div className="phone-info">
                <h5>
                  Allan BUSI :
                  <span>
                    <FontAwesomeIcon icon={faPhone} alt="icon telephone" style={{ margin: "0 5px 0 25px", width: 20, color: "white" }} />{" "}
                    <a href="tel:0771023973">07 71 02 39 73</a>
                  </span>
                </h5>
              </div>

              <div className="phone-info">
                <h5>
                  Adrien MAILLARD :
                  <span>
                    <FontAwesomeIcon icon={faPhone} alt="icon telephone" style={{ margin: "0 5px 0 25px", width: 20, color: "white" }} />{" "}
                    <a href="tel:0627180907">06 27 18 09 07</a>
                  </span>
                </h5>
              </div>
            </div>
          </div>
          <div className="col-lg-6 wow fadeInRight" data-wow-duration="0.5s" data-wow-delay="0.25s">
            <form id="contact" onSubmit={onSubmit}>
              {!contactSuccess && (
                <>
                  <div style={{ textAlign: "end" }}>
                    <span>*</span> : Champs requis
                  </div>
                  <div className="row">
                    <div className="col-lg-6">
                      <fieldset>
                        <label>
                          Prénom <span>*</span>
                        </label>
                        <input
                          style={{ borderRadius: 5 }}
                          onChange={onChange}
                          type="text"
                          value={state.prenom}
                          name="prenom"
                          id="surname"
                          placeholder="Votre prénom"
                          autoComplete="on"
                          required
                        />
                      </fieldset>
                    </div>
                    <div className="col-lg-6">
                      <fieldset>
                        <label>
                          Nom <span>*</span>
                        </label>
                        <input
                          style={{ borderRadius: 5 }}
                          onChange={onChange}
                          type="text"
                          value={state.nom}
                          name="nom"
                          id="name"
                          placeholder="Votre nom"
                          autoComplete="on"
                          required
                        />
                      </fieldset>
                    </div>
                    <div className="col-lg-12">
                      <fieldset>
                        <label>
                          Courriel <span>*</span>
                        </label>
                        <input
                          style={{ borderRadius: 5 }}
                          onChange={onChange}
                          type="email"
                          value={state.email}
                          name="email"
                          id="email"
                          pattern="[^ @]*@[^ @]*"
                          placeholder="Votre courriel"
                          required
                        />
                      </fieldset>
                    </div>
                    <div className="col-lg-12">
                      <fieldset>
                        <label>Téléphone portable</label>
                        <input
                          style={{ borderRadius: 5 }}
                          onChange={onChange}
                          type="number"
                          onKeyPress={e => {
                            if (state.tel.length === 10) {
                              e.preventDefault();
                            }
                          }}
                          value={state.tel}
                          name="tel"
                          id="tel"
                          placeholder="0xxxxxxxxx"
                        />
                      </fieldset>
                    </div>
                    <div className="col-lg-12">
                      <fieldset>
                        <label>
                          Message <span>*</span>
                        </label>
                        <div style={{ marginBottom: 5, textAlign: "end" }}>
                          <small>{state.message.length} /4000</small>
                        </div>
                        <textarea
                          style={{ borderRadius: 5 }}
                          onChange={onChange}
                          name="message"
                          value={state.message}
                          className="form-control"
                          id="message"
                          maxLength={4000}
                          placeholder="Votre message"
                          required
                        ></textarea>
                      </fieldset>
                    </div>

                    <Suspense fallback={<div />}>
                      <>
                        <ReCAPTCHA
                          ref={recaptchaRef}
                          sitekey={process.env.NEXT_PUBLIC_CAPTCHA}
                          onChange={e => {
                            setState(prev => ({ ...prev, captcha: true }));
                          }}
                        />
                        {!state.captcha && <div className="captcha-validation">Veuillez valider le Captcha</div>}
                      </>
                    </Suspense>
                  </div>
                </>
              )}

              <div className="col-lg-12">
                {contactSuccess ? (
                  <Lottie
                    alt="Animation indiquant la bonne récéption du courriel"
                    animationData={animationEmail}
                    autoPlay
                    loop={false}
                    style={{ width: 100, margin: "0 auto" }}
                  />
                ) : (
                  <div style={{ textAlign: "center", marginTop: 25 }}>
                    {contactLoad ? (
                      <Loader type="TailSpin" color="#ff3440" height={50} width={50} />
                    ) : (
                      <button
                        disabled={!state.captcha}
                        type="submit"
                        id="form-submit"
                        className="main-button"
                        style={{
                          width: "50%",
                          borderRadius: 5,
                          fontWeight: "bold",
                          opacity: state.captcha ? 1 : 0.5,
                          cursor: state.captcha ? "pointer" : "not-allowed",
                        }}
                      >
                        Envoyer
                      </button>
                    )}
                  </div>
                )}
              </div>

              {/* <div className="contact-dec">
                    <img src="assets/images/contact-decoration.png" alt="contact décoration" />
                  </div> */}
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Contact;
